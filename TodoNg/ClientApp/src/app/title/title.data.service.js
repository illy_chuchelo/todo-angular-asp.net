var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
let TitleDataService = class TitleDataService {
    constructor(http) {
        this.http = http;
        this.url = "/api/titles";
    }
    getTitles() {
        return this.http.get(this.url);
    }
    getTitle(id) {
        return this.http.get(this.url + '/' + id);
    }
    createTtile(title) {
        return this.http.post(this.url, title);
    }
    updateTitle(title) {
        return this.http.put(this.url, title);
    }
    deleteTitle(id) {
        return this.http.delete(this.url + '/' + id);
    }
};
TitleDataService = __decorate([
    Injectable()
], TitleDataService);
export { TitleDataService };
//# sourceMappingURL=title.data.service.js.map