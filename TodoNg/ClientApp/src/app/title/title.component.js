var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TitleDataService } from './title.data.service';
import { TaskDataService } from 'src/app/task/task.data.service';
import { Title } from './title';
import { Task } from '../task/task';
let TitleComponent = class TitleComponent {
    constructor(TitleDataService, TaskDataService) {
        this.TitleDataService = TitleDataService;
        this.TaskDataService = TaskDataService;
        this.title = new Title; // изменяемый заголовок
        this.task = new Task; // изменяемый подзаголовок
        this.tableMode = true;
    }
    ngOnInit() {
        setTimeout(() => this.loadTitles(), 1000); // загрузка данных при старте компонента   
    }
    //////////******TITLES******/////////
    // получаем данные через сервис
    loadTitles() {
        this.TitleDataService.getTitles().subscribe((data) => this.titles = data);
        this.TitleDataService.getTitles().subscribe((data) => this.todo = data.filter(x => x.done == false));
        this.TitleDataService.getTitles().subscribe((data) => this.done = data.filter(x => x.done == true));
        console.log(this.titles);
    }
    // сохранение данных
    saveTitle() {
        if (this.title.id == null) {
            if (this.title.name != null)
                this.TitleDataService.createTtile(this.title)
                    .subscribe(data => this.loadTitles());
        }
        else {
            this.TitleDataService.updateTitle(this.title)
                .subscribe(data => this.loadTitles());
        }
        this.cancel();
    }
    editTitle(t) {
        this.title = t;
    }
    cancel() {
        this.title = new Title();
        this.task = new Task();
        this.tableMode = true;
    }
    deleteTitle(t) {
        this.TitleDataService.deleteTitle(t.id)
            .subscribe(data => this.loadTitles());
    }
    add() {
        this.cancel();
    }
    /////////****************///////////
    ////////****TASKS******/////////////
    saveTask(t) {
        if (this.task.id == null) {
            if (this.task.name != null) {
                this.task.titleid = t.id;
                this.TaskDataService.createTask(this.task)
                    .subscribe();
            }
        }
        else {
            this.TaskDataService.updateTask(this.task)
                .subscribe();
        }
        this.cancel();
    }
    editTaskk(ta) {
        this.task = ta;
    }
    deleteTask(ta) {
        this.TaskDataService.deleteTask(ta.id)
            .subscribe(data => this.loadTitles());
    }
    toggleTaskDone(ta) {
        this.task = ta;
        this.task.done = !this.task.done;
        this.TaskDataService.updateTask(this.task).subscribe();
        this.cancel();
    }
    /////////****************///////////
    drop(event) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        }
        else {
            transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.title = event.container.data[0];
            if (event.previousContainer.id == "cdk-drop-list-0") {
                this.title.done = true;
            }
            else if (event.previousContainer.id == "cdk-drop-list-1") {
                this.title.done = false;
            }
            this.saveTitle();
        }
    }
};
TitleComponent = __decorate([
    Component({
        selector: 'app-title',
        templateUrl: './title.component.html',
        styleUrls: ['./title.component.css'],
        providers: [TitleDataService, TaskDataService]
    })
], TitleComponent);
export { TitleComponent };
//# sourceMappingURL=title.component.js.map