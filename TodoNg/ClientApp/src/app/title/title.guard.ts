﻿import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from '@angular/core';

@Injectable()
export class TitleGuard implements CanActivate
{

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    {
        const currentUser = localStorage.getItem("isLogin");

        if (currentUser)
        {              
            return true;
        }
        this.router.navigate(['/login']);
        return false;

    }

}