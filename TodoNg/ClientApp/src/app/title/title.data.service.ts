﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Title } from './title';

@Injectable()
export class TitleDataService
{
    private url = "/api/titles";

    constructor(private http: HttpClient)
    { }

    getTitles()
    {
        return this.http.get(this.url);
    }

    getTitle(id: number)
    {
        return this.http.get(this.url + '/' + id);
    }

    createTtile(title: Title)
    {
        return this.http.post(this.url, title);
    }

    updateTitle(title: Title)
    {
        return this.http.put(this.url, title);
    }

    deleteTitle(id: number)
    {
        return this.http.delete(this.url + '/' + id);
    }
}