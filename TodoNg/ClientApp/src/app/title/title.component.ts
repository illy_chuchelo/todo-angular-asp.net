﻿import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TitleDataService } from './title.data.service';
import { TaskDataService } from 'src/app/task/task.data.service';
import { NgForm } from '@angular/forms';
import { Title } from './title';
import { Task } from '../task/task';


@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
    styleUrls: ['./title.component.css'],
    providers: [TitleDataService, TaskDataService]
})
export class TitleComponent implements OnInit
{
    title: Title = new Title;    // изменяемый заголовок
    titles: Title[];            // массив заголовков 

    task: Task = new Task;      // изменяемый подзаголовок

    todo;
    done;

    tableMode: boolean = true;  
    
    constructor(private TitleDataService: TitleDataService, private TaskDataService: TaskDataService) {  }

    ngOnInit(): void
    {         
        setTimeout(() => this.loadTitles(), 1000);  // загрузка данных при старте компонента   
        
    }

//////////******TITLES******/////////

    // получаем данные через сервис
     loadTitles()
    {
         this.TitleDataService.getTitles().subscribe((data: Title[]) => this.titles = data);
         this.TitleDataService.getTitles().subscribe((data: Title[]) => this.todo = data.filter(x => x.done == false));
         this.TitleDataService.getTitles().subscribe((data: Title[]) => this.done = data.filter(x => x.done == true));
         console.log(this.titles)
    }

    

    // сохранение данных
    saveTitle()
    {
        if (this.title.id == null )
        {
            if (this.title.name != null)
                this.TitleDataService.createTtile(this.title)
                .subscribe(data => this.loadTitles()); 
        }
        else
        {            
            this.TitleDataService.updateTitle(this.title)
                .subscribe(data => this.loadTitles());
        }
        this.cancel();
    }

    editTitle(t: Title)
    {
        this.title = t;        
    }

    cancel()
    {
        this.title = new Title();
        this.task = new Task();
        this.tableMode = true;
    }

    deleteTitle(t: Title)
    {
        this.TitleDataService.deleteTitle(t.id)
            .subscribe(data => this.loadTitles());
    }

    add()
    {
        this.cancel();         
    }
/////////****************///////////

////////****TASKS******/////////////
    saveTask(t: Title)
    {        
        if (this.task.id == null)
        {
            if (this.task.name != null)
            {
                this.task.titleid = t.id;
                this.TaskDataService.createTask(this.task)
                    .subscribe(data => this.loadTitles());
            }                
        }
        else
        {
            this.TaskDataService.updateTask(this.task)
                .subscribe(data => this.loadTitles());
        }
        this.cancel();
    }

    editTaskk(ta: Task)
    {
        this.task = ta;
    }

    deleteTask(ta: Task)
    {
        this.TaskDataService.deleteTask(ta.id)
            .subscribe(data => this.loadTitles());
    }

    toggleTaskDone(ta: Task)
    {
        this.task = ta;
        this.task.done = !this.task.done;
        this.TaskDataService.updateTask(this.task).subscribe();
        this.cancel();

    }

/////////****************///////////

    drop(event: CdkDragDrop<string[]>)
    {
        if (event.previousContainer === event.container)
        {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            
        }
        else
        {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex,
            );
            this.title = event.container.data[0] as Title;
            if (event.previousContainer.id == "cdk-drop-list-0")
            {
                this.title.done = true;
            }
            else if (event.previousContainer.id == "cdk-drop-list-1")
            {
                this.title.done = false
            }            
            this.saveTitle();
            
        }
    }

}
