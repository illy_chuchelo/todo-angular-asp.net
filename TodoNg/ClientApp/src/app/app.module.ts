import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule }   from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatBadgeModule} from '@angular/material/badge';
import {MatIconModule} from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';




import { NavComponent } from './nav/nav.component';
import { TitleComponent } from './title/title.component';
import { TaskComponent } from './task/task.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoginGuard } from './login/login.guard';
import { TitleGuard } from './title/title.guard';




// ����������� ���������
const appRoutes: Routes = [
    { path: '', component: TitleComponent, canActivate: [TitleGuard] }, 
    { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
    { path: 'register', component: RegisterComponent, canActivate: [LoginGuard] },
    { path: '**', redirectTo: '/' }
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TitleComponent,
    TaskComponent,
    LoginComponent,
    RegisterComponent
          
  ],

  imports: [  
    BrowserModule,  
    BrowserAnimationsModule,    
    DragDropModule,
    MatDialogModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    MatListModule,
    MatCheckboxModule,
    MatBadgeModule,
    MatIconModule,
    MatToolbarModule,
    HttpClientModule,
    
    RouterModule.forRoot(appRoutes)
  ],

  exports: [        
    DragDropModule, 
    FormsModule,
    AppComponent   
  ],
   providers: [LoginGuard, TitleGuard],
  bootstrap: [AppComponent]
})
export class AppModule
{
}
