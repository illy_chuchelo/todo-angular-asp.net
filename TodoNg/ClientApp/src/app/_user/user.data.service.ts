﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable()
export class UserDataService {
    private url = "/api/auth";

    constructor(private http: HttpClient) { }


    loginUser(user: User)
    {
        return this.http.post(this.url + '/login', user);
    }

    registerUser(user: User)
    {
        return this.http.post(this.url + '/register', user);
    }

    logoutUser()
    {
        return this.http.post(this.url + '/logout', null);
    }

}