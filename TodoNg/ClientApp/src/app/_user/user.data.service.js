var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
let UserDataService = class UserDataService {
    constructor(http) {
        this.http = http;
        this.url = "/api/auth";
    }
    loginUser(user) {
        return this.http.post(this.url + '/login', user);
    }
    registerUser(user) {
        return this.http.post(this.url + '/register', user);
    }
    logoutUser() {
        return this.http.post(this.url + '/logout', null);
    }
};
UserDataService = __decorate([
    Injectable()
], UserDataService);
export { UserDataService };
//# sourceMappingURL=user.data.service.js.map