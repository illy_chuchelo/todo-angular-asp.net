var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { UserDataService } from 'src/app/_user/user.data.service';
let NavComponent = class NavComponent {
    constructor(UserDataService, router) {
        this.UserDataService = UserDataService;
        this.router = router;
        this.currentUser = localStorage.getItem("isLogin");
    }
    logout() {
        this.UserDataService.logoutUser().subscribe();
        localStorage.clear();
        this.router.navigate(['/login']);
    }
};
NavComponent = __decorate([
    Component({
        selector: 'app-nav',
        templateUrl: './nav.component.html',
        styleUrls: ['./nav.component.css'],
        providers: [UserDataService]
    })
], NavComponent);
export { NavComponent };
//# sourceMappingURL=nav.component.js.map