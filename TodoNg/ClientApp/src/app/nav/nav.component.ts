import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { UserDataService } from 'src/app/_user/user.data.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers: [UserDataService]
})
export class NavComponent
{
    
    constructor(private UserDataService: UserDataService, private router: Router) { }

    currentUser = localStorage.getItem("isLogin")

    logout()
    {
        this.UserDataService.logoutUser().subscribe();
        localStorage.clear();
        this.router.navigate(['/login']);
    }
   
}
