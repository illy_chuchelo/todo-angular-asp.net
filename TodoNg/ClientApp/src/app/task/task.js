export class Task {
    constructor(id, name, done, titleid) {
        this.id = id;
        this.name = name;
        this.done = done;
        this.titleid = titleid;
    }
}
//# sourceMappingURL=task.js.map