﻿import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Injectable } from '@angular/core';

@Injectable()
export class LoginGuard implements CanActivate
{   
    
    constructor( private router: Router)  {  }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const currentUser = localStorage.getItem("isLogin");        

        if (currentUser)
        {            
            this.router.navigate(['/']);
            return false;
        }                    
            return true;                  

    }

}