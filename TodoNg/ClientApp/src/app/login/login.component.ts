import { Component } from '@angular/core';
import {  Router } from "@angular/router";
import { UserDataService } from 'src/app/_user/user.data.service';
import { User } from 'src/app/_user/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserDataService]
})
export class LoginComponent {

    user: User = new User;    
    error;
        
    constructor(private UserDataService: UserDataService, private router: Router) { }


    login()
    {
        if (this.user.email && this.user.password)
        {            
            this.UserDataService.loginUser(this.user).subscribe(
                (data: User) =>
                {
                    if (data.email != undefined)
                    {
                        localStorage.setItem("isLogin", "true");
                        this.router.navigate(['/']);
                    }
                },
                (err) => { this.error = err.error["1"] });
            
        }
        
    }


}
