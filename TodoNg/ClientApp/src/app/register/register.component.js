var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { UserDataService } from 'src/app/_user/user.data.service';
import { User } from 'src/app/_user/user';
let RegisterComponent = class RegisterComponent {
    constructor(UserDataService, router) {
        this.UserDataService = UserDataService;
        this.router = router;
        this.user = new User;
    }
    register() {
        if (this.user.email && this.user.password && this.user.name) {
            this.UserDataService.registerUser(this.user).subscribe((data) => {
                if (data.email != undefined) {
                    this.router.navigate(['/']);
                }
            }, (err) => { this.error = err.error["2"]; });
        }
    }
};
RegisterComponent = __decorate([
    Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css'],
        providers: [UserDataService]
    })
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map