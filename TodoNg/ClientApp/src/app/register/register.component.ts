import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { UserDataService } from 'src/app/_user/user.data.service';
import { User } from 'src/app/_user/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserDataService]
})
export class RegisterComponent
{
    user: User = new User; 
    error


    constructor(private UserDataService: UserDataService, private router: Router) { }


    register()
    {
        if (this.user.email && this.user.password && this.user.name)
        {
            this.UserDataService.registerUser(this.user).subscribe(
              (data: User) =>
                {
                    if (data.email != undefined)
                    {                        
                        this.router.navigate(['/']);
                    }
                },
                (err) => { this.error = err.error["2"] });
        }
        
    }
}
