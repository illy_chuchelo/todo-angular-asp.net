﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TodoNg.Models;
using TodoNg.Models.Data;

namespace TodoNg.Controllers
{
    [Route("api/tasks")]
    [ApiController]
    public class TaskController : Controller
    {
        private ApplicationContext db = new ApplicationContext();


        
        [HttpGet("{id}")]
        public Task Get(int id)
        {
            Task task = db.Tasks.FirstOrDefault(x => x.Id == id);
            return task;
        }

        
        [HttpPost]
        public IActionResult Post(Task task)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(task);
                db.SaveChanges();
                return Ok(task);
            }
            return BadRequest(ModelState);
        }

        
        [HttpPut]
        public IActionResult Put(Task task)
        {
            if (ModelState.IsValid)
            {
                db.Update(task);
                db.SaveChanges();
                return Ok(task);
            }
            return BadRequest(ModelState);
        }

        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Task task = db.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                db.Tasks.Remove(task);
                db.SaveChanges();
            }
            return Ok(task);
        }
    }
}
