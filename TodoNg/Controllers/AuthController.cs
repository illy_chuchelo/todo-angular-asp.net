﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TodoNg.Models;
using TodoNg.Models.Data;
using Task = System.Threading.Tasks.Task;

namespace TodoNg.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : Controller
    {
        private ApplicationContext db = new ApplicationContext();


        [HttpPost, Route("login")]     
        public async Task<IActionResult> Login(User user)
        {
            if (ModelState.IsValid)
            {
                User _user = await db.Users.FirstOrDefaultAsync(u => u.Email == user.Email && u.Password == user.Password);
                if (_user != null)
                {
                    await Authenticate(user.Email); // аутентификация

                    return Ok(user);
                }
                ModelState.AddModelError("1", "Некорректные логин и(или) пароль");
            }
            return BadRequest(ModelState);
        }


        [HttpPost, Route("register")]        
        public async Task<IActionResult> Register(User user)
        {
            if (ModelState.IsValid)
            {
                User _user = await db.Users.FirstOrDefaultAsync(u => u.Email == user.Email);
                if (_user == null)
                {
                    // добавляем пользователя в бд
                    db.Users.Add(user);
                    await db.SaveChangesAsync();

                    //await Authenticate(user.Email); // аутентификация

                    return Ok(user);
                }
                else
                    ModelState.AddModelError("2", "Такой логин уже зарегестрирован");
            }
            return BadRequest(ModelState);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        [HttpPost, Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }
    }
}

