﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TodoNg.Models;
using TodoNg.Models.Data;

namespace TodoNg.Controllers
{
    [Route("api/titles")]
    [ApiController]
    
    public class TitleController : Controller
    {
        private ApplicationContext db = new ApplicationContext();
                
        // GET: api/Title
        [HttpGet]
        public async Task<IEnumerable<Title>> Get()
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

            return  db.Titles
                .Where(ti => ti.UserId == user.Id)
                .Include(t => t.Tasks)
                .ToList();
        }

        // GET: api/Title/5
        [HttpGet("{id}")]
        public Title Get(int id)
        {
            Title title = db.Titles.FirstOrDefault(x => x.Id == id);
            return title;
        }

        // POST: api/Title
        [HttpPost]
        public IActionResult Post(Title title)
        {
            if (ModelState.IsValid)
            {
                User user = db.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
                title.UserId = user.Id;
                db.Titles.Add(title);
                db.SaveChanges();
                return Ok(title);
            }
            return BadRequest(ModelState);
        }

        // PUT: api/Title/5
        [HttpPut]
        public IActionResult Put(Title title)
        {
            if (ModelState.IsValid)
            {
                db.Update(title);
                db.SaveChanges();
                return Ok(title);
            }
            return BadRequest(ModelState);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Title title = db.Titles.FirstOrDefault(x => x.Id == id);
            if (title != null)
            {
                db.Titles.Remove(title);
                db.SaveChanges();
            }
            return Ok(title);
        }
    }
}
