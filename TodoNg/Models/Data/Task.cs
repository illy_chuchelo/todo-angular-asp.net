﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoNg.Models.Data
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; } // Название Действия
        public bool Done { get; set; }

        public int TitleId { get; set; } // TitleId == Title.Id
        public Title Title { get; set; } // Действие имеет одно Название Списка
    }
}
