﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoNg.Models.Data
{
    public class Title
    {
        public int Id { get; set; }
        public string Name { get; set; } // Название Списка
        public bool Done { get; set; }

        public int UserId { get; set; } // Ключь для связи UserId == User.Id 
        public User User { get; set; } // Название Списка имеет одного Пользователя

        public List<Task> Tasks { get; set; } // Название Списка имеет много Действий
    }
}
