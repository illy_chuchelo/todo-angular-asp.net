﻿using Microsoft.EntityFrameworkCore;
using TodoNg.Models.Data;

namespace TodoNg.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Title> Titles { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<User> Users { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=localhost;UserId=root;Password=1820;database=todo_ng;");
        }
    }
}
